$(document).ready(function() {

var clock;
clock = $(".clock").FlipClock({
	clockFace : "DailyCounter",
	autoStart : false,
	callbacks : {
		stop : function() {
			$(".message").html("Врёмя прошло");
		}
	}
});
clock.SetTime(500);
clock.setCountdown(true);
clock.start();